package src.object;

/**
 * Мой объект,который требуется только для демонстрации.
 *
 * @author Matveev Alexander
 */
public class MyBestObject {

    /**
     * Поле класса.
     */
    private Boolean isBig;

    private String someText;

    /**
     * Получение поля.
     *
     * @return {@link Boolean}
     */
    public Boolean getBig() {
        return isBig;
    }

    /**
     * Установка поля.
     *
     * @param big большой?
     */
    public void setBig(Boolean big) {
        isBig = big;
    }

    /**
     * Получение какого-то текста.
     *
     * @return какой-то текст
     */
    public String getSomeText() {
        return someText;
    }

    /**
     * Конструктор класса.
     *
     * @param isBig большой?
     * @param someText какой-то текст
     */
    public MyBestObject(Boolean isBig, String someText) {
        this.isBig = isBig;
        this.someText = someText;
    }

    @Override
    public String toString() {
        String str = "Объект с текстом \"" + someText + "\". И он";
        String sizeText = isBig ? " большущий." : " не очень большой";
        return str + sizeText;
    }
}
