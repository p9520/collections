package src.main;

import src.object.MyBestObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        //Здесь можно потыкать по классам в IDE. Ctrl + левый клик по интерфейсу или классу
        List list = new ArrayList();
        Queue queue = new LinkedList();
        Set set = new HashSet();
        Map map = new HashMap();


        List<MyBestObject> myBestObjects = new ArrayList<>();

        MyBestObject house = new MyBestObject(true, "улица Креветочная дом 5");
        myBestObjects.add(house);
        MyBestObject cow = new MyBestObject(false, "Бурёнка");
        myBestObjects.add(cow);
        MyBestObject salary = new MyBestObject(false, "Зарплата");
        myBestObjects.add(salary);

        System.out.println("Обход с помощью итератора");
        Iterator<MyBestObject> iterator = myBestObjects.iterator();
        while (iterator.hasNext()) {
            MyBestObject objectFromIterator = iterator.next();
            System.out.println(objectFromIterator);
        }

        System.out.println("Обход через for");
        for (MyBestObject obj : myBestObjects) {
            System.out.println(obj);
        }

        System.out.println("Обход с помощью Stream Api");
        myBestObjects.stream()
                .forEach(myBestObject -> System.out.println(myBestObject));
    }
}
